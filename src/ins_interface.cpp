#include "ins_interface.h"

#include <boost/thread.hpp>
#include <std_msgs/String.h>

namespace ins {

Interface::Interface(ros::NodeHandle &nh, ros::NodeHandle &p_nh)
{
  p_nh.param<std::string>("port", _port, "/dev/ttyUSB1");
  p_nh.param<int>("baudrate", _baudrate, 115200);
  p_nh.param<bool>("use_serial", _use_serial, true);
  _pub_gpgga = nh.advertise<std_msgs::String>("ntrip/gpgga", 100);
  _sub_ntrip = nh.subscribe<std_msgs::String>("ntrip/ntrip", 100, &Interface::cbNtrip, this);

//  _diag_updater.setHardwareID(HARDWARD_ID);
//  _diag_updater.add("Ins Driver Status", this, &Interface::diagnosticStatus);

//  _diag_timer = p_nh.createTimer(ros::Duration(0.1), &Interface::cbDiagTimer,this);

  _last_serial_time = ros::Time::now();
}

bool Interface::openSerial() {
  _p_serial = SerialPtr(new serial_port(_io_serial));
  _p_serial->open(_port, _serial_err);
  if(_serial_err) {
    ROS_WARN_STREAM("[InsInterface] serial open error: " << _serial_err.message().c_str());
    return false;
  }

  _p_serial->set_option(boost::asio::serial_port_base::baud_rate(_baudrate));
  _p_serial->set_option(boost::asio::serial_port_base::character_size(8));
  _p_serial->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
  _p_serial->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
  _p_serial->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

  recvSerial();
  boost::thread serial_thread(boost::bind(&boost::asio::io_service::run, &_io_serial));
  return true;
}

void Interface::recvSerial()
{
  if(_p_serial == nullptr || !_p_serial->is_open()) return;
  boost::asio::async_read_until(
        *_p_serial, _buf, '\r',
        boost::bind(&Interface::cbRecvSerial, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred)
        );
}

void Interface::cbRecvSerial(const boost::system::error_code& e, std::size_t size)
{
  _serial_err = e;
  if (!e) {
    std::istream is(&_buf);
    std::string line;
    std::getline(is, line);
    if(line.find("$GPGGA") != std::string::npos) {
      _last_serial_time = ros::Time::now();

      std_msgs::String msg;
      msg.data = line;
      _pub_gpgga.publish(msg);
      //      ROS_INFO_STREAM(line);
    }
  }
//  else {
//    ROS_WARN_STREAM("[OxfordIns::recvSerial] serial receive error: " << e.message().c_str());
//  }
  recvSerial();
}

void Interface::cbNtrip(const std_msgs::StringConstPtr& msg)
{
  std::string data = msg->data;
  boost::asio::async_write(*_p_serial,
                           boost::asio::buffer(data, data.size()),
                           boost::bind(&Interface::cbSendSerial, this,
                                       boost::asio::placeholders::error,
                                       boost::asio::placeholders::bytes_transferred));
}

void Interface::cbSendSerial(const boost::system::error_code& e, std::size_t size)
{
  _serial_err = e;
  if (e) {
    ROS_ERROR_STREAM("[OxfordIns::sendSerial] send error: " << e.message().c_str());
  }
  else {
//    ROS_INFO_STREAM("[OxfordIns::sendSerial] serial write ntrip data: " << size << " bytes");
  }
}

//void Interface::diagnosticStatus(diagnostic_updater::DiagnosticStatusWrapper &stat)
//{
//  ros::Time t = ros::Time::now();
//  double udp_duration = (t - _last_udp_time).toSec();
//  bool udp_freq_ok = (udp_duration < 0.02); // 20ms, 데이터 수신 주기 = 100hz (10ms)
////  udp_freq_ok = true;  // for debug

//  double serial_duration = (t - _last_serial_time).toSec();
//  bool serial_freq_ok = (serial_duration < 2.0); // 2s, 데이터 수신 주기 = 1hz (1s)
////  serial_freq_ok = true;  // for debug

//  bool udp_open_ok = (_p_udp != nullptr && _p_udp->is_open());
//  bool serial_open_ok = (_p_serial != nullptr && _p_serial->is_open());

//  bool all_ok;
//  if(_use_serial)
//    all_ok = udp_open_ok & udp_freq_ok & !_udp_err & serial_open_ok & serial_freq_ok & !_serial_err;
//  else
//    all_ok = udp_open_ok & udp_freq_ok & !_udp_err;

//  if(all_ok)
//    stat.summary(diagnostic_msgs::DiagnosticStatus::OK, "INS Interface OK");
//  else
//    stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "INS Interface Error");

//  if(_udp_err)
//    stat.add("UDP Error", _udp_err.message());
//  else if(!udp_open_ok)
//    stat.add("UDP Error", "Open Error");
//  else if(!udp_freq_ok)
//    stat.add("UDP Error", "Timeout");
//  else
//    stat.add("UDP Error", "No Error");

//  if(_use_serial) {
//    stat.add("Use Serial", "True");
//    if(_serial_err)
//      stat.add("Serial Error", _serial_err.message());
//    else if(!serial_open_ok)
//      stat.add("Serial Error", "Open Error");
//    else if(!serial_freq_ok)
//      stat.add("Serial Error", "Timeout");
//    else
//      stat.add("Serial Error", "No Error");
//  }
//  else {
//    stat.add("Use Serial", "False");
//    stat.add("Serial Error", "No Error");
//  }
//}

//void Interface::cbDiagTimer(const ros::TimerEvent &event)
//{
//  (void)event;
//  // Call necessary to provide an error when no velodyne packets are received
//  _diag_updater.force_update();
//}

}
