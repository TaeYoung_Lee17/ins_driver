#include <ros/ros.h>

#include "ins_interface.h"


int main(int argc, char **argv)
{
  // Set up ROS.
  ros::init(argc, argv, "ins_interface_node");

  // read parameter
  ros::NodeHandle nh;
  ros::NodeHandle p_nh("~");

  ins::Interface net(nh, p_nh);
//  if(net.openUdp())
//    ROS_INFO("UDP open OK");
  if(net.openSerial())
    ROS_INFO("Serial open OK");

  // main loop
  ros::spin();
  ROS_INFO("ins interface close");

  return 0;
}
