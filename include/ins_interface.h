#ifndef INTERFACE_H
#define INTERFACE_H

#include <ros/ros.h>
#include <boost/asio.hpp>

#include <std_msgs/String.h>

//#include <diagnostic_updater/diagnostic_updater.h>

namespace ins {

using boost::asio::ip::udp;
using boost::asio::serial_port;

class Interface
{
  typedef boost::shared_ptr<serial_port> SerialPtr;

//  diagnostic_updater::Updater _diag_updater;
  ros::Timer _diag_timer;
  boost::system::error_code _serial_err;

  std::string _port;
  int _baudrate;
  bool _use_serial;

  boost::asio::io_service _io_serial;
  SerialPtr _p_serial;
  boost::asio::streambuf _buf;

  ros::Publisher _pub_raw;
  ros::Publisher _pub_gpgga;
  ros::Subscriber _sub_ntrip;

  ros::Time _last_serial_time;

public:
  Interface(ros::NodeHandle &nh, ros::NodeHandle &p_nh);

  bool open();
//  void close();
  bool openSerial();
private:
  void recvSerial();
  void cbRecvSerial(const boost::system::error_code &e, std::size_t size);

  void cbNtrip(const std_msgs::StringConstPtr &msg);
  void cbSendSerial(const boost::system::error_code &e, std::size_t size);

//  void diagnosticStatus(diagnostic_updater::DiagnosticStatusWrapper &stat);
//  void cbDiagTimer(const ros::TimerEvent &event);
};

}
#endif // INTERFACE_H
